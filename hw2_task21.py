# შექმენით 10 ელემენტიანი სია, რომლის ელემენტებია ნებისმიერი შემთხვევითი
# მთელი რიცხვები 25-დან 110-მდე. დაბეჭდეთ სია და იპოვეთ მინიმალური ელემენტი.

import random
mylist = []
for i in range(0, 10):
    mylist.append(random.randint(25, 110))

print(mylist)
print(min(mylist))
