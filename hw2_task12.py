# დაწერეთ ფუნქცია, რომელიც დაითვლის (დააბრუნებს) პარამეტრად გადაცემული
# რიცხვის ფაქტორიალს და დაბეჭდეთ შედეგი სხვადასხვა რიცხვებისთვის.

import math

def factcalc(num):
    return math.factorial(num)

print(factcalc(12))
print(factcalc(7))
print(factcalc(11))

