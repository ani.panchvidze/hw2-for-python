# დაწერეთ ფუნქცია, რომელიც შეამოწმებს პარამეტრად გადაცემული რიცხვი არის
# თუ არა კენტი. თუ კენტია, დააბრუნოს მნიშვნელობა True, თუ არადა - False.
# შეამოწმეთ რამდენიმე რიცხვისთვის და დაბეჭდეთ შედეგი.

def oddeven(number):
    return False if number % 2 == 0 else True


print(oddeven(123))
print(oddeven(967))
print(oddeven(12))