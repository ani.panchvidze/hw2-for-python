#შექმენით სია fruits, რომელის ელემენტებია: Watermelon, Banana, Apple. დაალაგეთ
# სიის ელემენტები ალფაბეტის უკუ-მიმართულებით და დაბეჭდეთ ისინი.

x = ['Banana', 'Watermelon', 'Apple']

print(sorted(x, reverse=True))