# დაწერეთ ფუნქცია, რომელსაც არგუმენტად გადაეცემა 2 სია. ფუნქცია აბრუნებს
# მნიშვნელობა True-ს თუ სიებს აქვთ ერთი მაინც საერთო ელემენტი. წინააღმდეგ
# შემთხვევაში აბრუნებს False მნიშვნელობას.

def twolists(a, b):
    check = False
    for i in a:
        if i in b:
            check = True
        return check

a = [1, 3, 4,5]
b = [1, 6, 7, 8]
print(twolists(a, b))

