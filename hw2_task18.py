# დაწერეთ ფუნქცია, რომელსაც არგუმენტად გადაეცემა სია, დაითვლის სიის
# ელემენტების ნამრავლს და დააბრუნებს შედეგს. გამოიძახეთ ფუნქცია
# ნებისმიერი სიისთვის.

def listtimes(mylist):
    result = 1
    for x in mylist:
       result = result*x
    return result

anylist = [2, 12, 1]
print(listtimes(anylist))


